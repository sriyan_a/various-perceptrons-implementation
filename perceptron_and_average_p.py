#!/usr/bin/env

import csv, pandas as pd, numpy as np
import matplotlib.pyplot as plt

def binarize_continious_data(data):
    #This fuction I create multiple ranges for all continious variables and each age to that group. 
    #After doing this I drop the existing column
    age_group = []
    for age in data["age"]:
        if age < 25:
            age_group.append("<25")
        elif 25 <= age <= 34:
            age_group.append("25-34")
        elif 34 < age <= 44:
            age_group.append("35-44")
        elif 44 < age <= 54:
            age_group.append("45-54")
        elif 54 < age <= 65:
            age_group.append("55-64")
        else:
            age_group.append("65 and over")

    income_df = data.copy()
    income_df["age_group"] = age_group
    del income_df["age"]
    
    workhours_group = []
    for workhours in income_df["workhours"]:
        if workhours < 25:
            workhours_group.append("<25")
        elif 26 <= workhours <= 50:
            workhours_group.append("26-50")
        elif 50 < workhours <= 75:
            workhours_group.append("51-75")
        else:
            workhours_group.append("76 and over")

    new_income_df = income_df.copy()
    new_income_df["workhours_group"] = workhours_group
    del new_income_df["workhours"]
    
    return new_income_df

def data_preprocessing(data):
    #Here I binarize all the continious and catagorical data. As I am binarizing I don't need a scalar fit.
    #I also return the values of x_train and y_train.
    #Also converting the 0 or 1 class in y_train to -1 or 1.
    data = binarize_continious_data(data)
    data = pd.get_dummies(data, drop_first=True)
    return data.loc[:, data.columns != 'income_ >50K'], data['income_ >50K'].map({0: -1, 1: 1})

def accuracy(w, X, Y, b=0):
    wx = np.dot(w, X.T)
    if b == 0:
        b = np.zeros_like(wx)
    y_hat = np.sign(wx + b)
    corr = np.sum((y_hat == np.array(Y)).astype(float))
    return round((corr/len(X.values)) * 100, 4)

def perceptron(itr, X, Y, X_dev, Y_dev, X_test, Y_test, pa=0):
    accuracy_train = []
    accuracy_test = []
    accuracy_dev = []
    mistakes = []

    w = np.zeros(len(X.columns))
    tau = 1

    for i in range(itr):
        count = 0
        
        for x, y in zip(X.values, Y):
            y_hat = np.sign(np.dot(w, x))
            if y_hat != y:
                count += 1
                if pa:
                    tau = float(1 - (y * np.dot(w, x))) / np.linalg.norm(x, ord=1)**2
                w = w + tau*y*x

        mistakes.append(count)
        accuracy_train.append(accuracy(w, X, Y))
        accuracy_dev.append(accuracy(w, X_dev, Y_dev))
        accuracy_test.append(accuracy(w, X_test, Y_test))
    
    return mistakes, accuracy_train, accuracy_dev, accuracy_test

def naive_average_perceptron(itr, X, Y, X_dev, Y_dev, X_test, Y_test):
    accuracy_train = []
    accuracy_test = []
    accuracy_dev = []
    
    w = np.zeros(len(X.columns))
    sum_w = np.zeros(len(X.columns))
    eta = 1 #learning rate
    count = 0
    
    for i in range(itr):
        for x, y in zip(X.values, Y):
            y_hat = np.sign(np.dot(w, x))
            if y_hat != y:
                w = w + eta*y*x
                sum_w += w
                count += 1
                
        average_w = sum_w / count
        
        accuracy_train.append(accuracy(w, X, Y))
        accuracy_dev.append(accuracy(w, X_dev, Y_dev))
        accuracy_test.append(accuracy(w, X_test, Y_test))
        
    return accuracy_train, accuracy_dev, accuracy_test

def average_perceptron_smart(itr, X, Y, X_dev, Y_dev, X_test, Y_test):
    accuracy_train = []
    accuracy_test = []
    accuracy_dev = []
    
    test_accuracy_fivek = []
    dev_accuracy_fivek = []
    
    w = np.zeros(len(X.columns))
    v = np.zeros(len(X.columns))
    train_count = 0
    b = 0
    a = 0
    count = 1
    
    for i in range(itr):
        for x, y in zip(X.values, Y):
            if y * (np.dot(w, x) + b) <= 0:
                w += y*x
                b += y
                v += y*count*x
                a += y*count
            count += 1
            
            train_count += 1
        
            if(train_count == (i+1)*5000):
                test_accuracy_fivek.append(accuracy((w-v/count), X_test, Y_test, (b-a/count)))
                dev_accuracy_fivek.append(accuracy((w-v/count), X_dev, Y_dev, (b-a/count)))
                
        train_count = 0
        
        accuracy_train.append(accuracy((w-v/count), X, Y, (b-a/count)))
        accuracy_test.append(accuracy((w-v/count), X_test, Y_test, (b-a/count)))
        accuracy_dev.append(accuracy((w-v/count), X_dev, Y_dev, (b-a/count)))
        
    return accuracy_train, accuracy_dev, accuracy_test, dev_accuracy_fivek, test_accuracy_fivek

def main():
    np.random.seed(10)
    
    #importing data
    cols = ["age", "workclass", "education", "relationship", "profession", "race", "gender", "workhours", "nationality", "income"]
    dev_data = pd.read_csv('income.dev.txt', header = None, names = cols)
    test_data = pd.read_csv('income.test.txt', header = None, names = cols)
    train_data = pd.read_csv('income.train.txt', header = None, names = cols)
    
    #Doing the data preprocessing over the given data
    data = pd.concat([train_data, dev_data, test_data])
    X, Y = data_preprocessing(data)
    
    x_train, y_train = X[:len(train_data)], Y[:len(train_data)]
    x_dev, y_dev = X[len(train_data):len(dev_data)+len(train_data)], Y[len(train_data):len(dev_data)+len(train_data)]
    x_test, y_test = X[len(dev_data)+len(train_data):], Y[len(dev_data)+len(train_data):]
    
    print('1) The number of features = ' + str(len(X.columns)))
    
    #Calling the perceptron algorithm and computing the accuracies and the mistakes of both
    itr = 5
    
    perceptron_mistakes_train, perceptron_accuracies_train, perceptron_accuracies_dev, perceptron_accuracies_test = perceptron(itr, x_train, y_train, x_dev, y_dev, x_test, y_test)
    
    print('4) For each iteration\n\tThe number of mistakes of perceptron for training set = ' + str(perceptron_mistakes_train) + '\n\tThe accuracies of perceptron for training set = ' + str(perceptron_accuracies_train))
    print('\n\tThe accuracies of perceptron for dev set = ' + str(perceptron_accuracies_dev))
    print('\n\tThe accuracies of perceptron for test set = ' + str(perceptron_accuracies_train))
    
    pa_mistakes_train, pa_accuracies_train, pa_accuracies_dev, pa_accuracies_test = perceptron(itr, x_train, y_train, x_dev, y_dev, x_test, y_test, 1)
    
    print('\n\tThe number of mistakes of pa algorithm for training set = ' + str(pa_mistakes_train) + '\n\tThe accuracies of pa algorithm for training set = ' + str(pa_accuracies_train))
    print('\n\tThe accuracies of pa algorithm for dev set = ' + str(pa_accuracies_dev))
    print('\n\tThe accuracies of pa algorithm for test set = ' + str(pa_accuracies_train))
    
    avg_accuracies_train, avg_accuracies_dev, avg_accuracies_test = naive_average_perceptron(itr, x_train, y_train, x_dev, y_dev, x_test, y_test)
    
    print('5)For each iteration\n\tThe accuracies of naive averaged perceptron for training set = ' + str(avg_accuracies_train)) 
    print('\n\tThe accuracies of naive averaged perceptron for dev set = ' + str(avg_accuracies_dev)) 
    print('\n\tThe accuracies of naive averaged perceptron for test set = ' + str(avg_accuracies_test))
    
    smart_avg_accuracies_train, smart_avg_accuracies_dev, smart_avg_accuracies_test, smart_avg_accuracies_dev_fivek, smart_avg_accuracies_test_fivek = average_perceptron_smart(itr, x_train, y_train, x_dev, y_dev, x_test, y_test)
    
    print('\n\tThe accuracies of smart averaged perceptron for train set = ' + str(smart_avg_accuracies_train))
    print('\n\tThe accuracies of smart averaged perceptron for dev set = ' + str(smart_avg_accuracies_dev))
    print('\n\tThe accuracies of smart averaged perceptron for test set = ' + str(smart_avg_accuracies_test))
    
    print('6)\n\tThe dev accuracy of the smart averaged perceptron for an incrimenting 5k values =' + str(smart_avg_accuracies_dev_fivek))
    print('\n\tThe test accuracy of the smart averaged perceptron for an incrimenting 5k values =' + str(smart_avg_accuracies_test_fivek))
    
    # iterations = [1,2,3,4,5]
    
    # plt.plot(iterations, perceptron_mistakes_train, color='black', label='Standard Perceptron', marker='o')
    # plt.plot(iterations, pa_mistakes_train, color='blue', label='PA Algorithm', marker='o')
    # plt.legend(loc='best')
    # plt.xlabel('No. of Iterations')
    # plt.ylabel('No. of Mistakes')
    # plt.title('Perceptron and PA Algorithm')
    # plt.show()
    
    # plt.plot(iterations, perceptron_accuracies_train, color='black', label='Train SP')
    # plt.plot(iterations, perceptron_accuracies_test, color='blue', label='Test SP')
    # plt.plot(iterations, perceptron_accuracies_dev, color='red', label='Dev SP')
    # plt.plot(iterations, pa_accuracies_train, color='black', label='Train PA', marker='o')
    # plt.plot(iterations, pa_accuracies_test, color='blue', label='Test PA', marker='o')
    # plt.plot(iterations, pa_accuracies_dev, color='red', label='Dev PA', marker='o')
    # plt.legend(loc='best')
    # plt.xlabel('No. of Iterations')
    # plt.ylabel('Accuracy')
    # plt.title('Accuracy of Standard Perceptron')
    # plt.show()
    
    # plt.plot(iterations, pa_accuracies_train, color='black', label='Train', marker='o')
    # plt.plot(iterations, pa_accuracies_test, color='blue', label='Test', marker='o')
    # plt.plot(iterations, pa_accuracies_dev, color='red', label='Dev')
    # plt.legend(loc='best')
    # plt.xlabel('No. of Iterations')
    # plt.ylabel('Accuracy')
    # plt.title('Accuracy of PA Algorithm')
    # plt.show()
    
    # plt.plot(iterations, smart_avg_accuracies_train, color='black', label='Train', marker='o')
    # plt.plot(iterations, smart_avg_accuracies_test, color='blue', label='Test', marker='o')
    # plt.plot(iterations, smart_avg_accuracies_dev, color='red', label='Dev')
    # plt.legend(loc='best')
    # plt.xlabel('No. of Iterations')
    # plt.ylabel('Accuracy')
    # plt.title('Accuracy of Average Perceptron')
    # plt.show()
    
    # x = [5000, 10000, 15000, 20000, 25000]
    # plt.plot(x, smart_avg_accuracies_test_fivek, color='blue', label='Testing', marker='o')
    # plt.plot(x, smart_avg_accuracies_dev_fivek, color='red', label='Dev', marker='o')
    # plt.legend(loc='best')
    # plt.xlabel('No. of Examples')
    # plt.ylabel('Accuracy')
    # plt.title('General Learning Curve for Averaged Classifier')
    # plt.show()
    # 

if __name__ == "__main__": 
    main()